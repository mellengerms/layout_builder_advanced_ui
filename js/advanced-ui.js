
(function ($, Drupal, drupalSettings) {

  var el = 'lb-ui';

  Drupal.layoutBuilderChanged = false;

  Drupal.behaviors.layoutBuilderAdvancedUiCheckChanges = {
    attach: function (context, settings) {
      Drupal.layoutBuilderChanged = $('[role="contentinfo"].messages--warning').length > 0;
      $('body')[Drupal.layoutBuilderChanged ? 'addClass' : 'removeClass']('js-' + el + '-changed');
      $('.button-save, .button-discard')[Drupal.layoutBuilderChanged ? 'removeClass' : 'addClass']('button-disabled');
    }
  };

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.layoutBuilderAdvancedUi = {
    attach: function (context, settings) {
      var layoutBuilder = $('#layout-builder');

      // Detect if layout builder is there.
      if ($('.layout-builder-form').length == 0) return;


      var template = '<div class="' + el + '"><div class="' + el + '__content">';
      template += '<a class="button button-save button-disabled" data-actionevent="click" data-actiontarget="#edit-submit">Save</a>';
      template += '<a class="button button-discard button-disabled" data-actionevent="click" data-actiontarget="#edit-discard-changes">Discard</a>';
      template += '<div class="' + el + '__preview"><label class="' + el + '__switch"><input type="checkbox" data-actionevent="change" data-actiontarget="#layout-builder-content-preview" /><span class="' + el + '__slider round"></span></label> Preview</div>';
      template += '<div class="' + el + '__revision"><input type="checkbox" data-actionevent="change" data-actiontarget="#edit-revision" checked id="' + el + '__revision" /><label for="' + el + '__revision">Create new revision</label></div>';
      template += '</div></div>';

      // $('#content').once('layou-builder-advanced-ui').append(template);
      $('.layout-builder-form').once('lb-ui').each(function () {
        $(template).insertBefore(this);
      });

      // Attach listeners.
      $('[data-actiontarget]').each(function () {
        var event = $(this).data('actionevent');

        if (event === 'change') {
          // Set up the same value as the target.
          var target = $(this).data('actiontarget');
          var targetValue = $(target).prop('checked');
          $(this).prop('checked', targetValue);
          $(this).change(function () {
            $(target).prop("checked", this.checked);
            $(target).trigger('change');
          });

        }
        else {
          $(this).on(event, function (e) {
            console.log(e);
            var target = $(this).data('actiontarget');
            var event = $(this).data('actionevent');
            $(target).trigger(event);
          });
        }
      });

      // // Observe if layout builder has changes.
      // MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

      // var observer = new MutationObserver(function (mutations, observer) {
      //   if ($('#layout-builder>div[data-drupal-messages]:first-child').length > 0) {
      //     $('.' + el + ' a[data-actionevent]').removeClass('button-disabled');
      //     // Minus 10 is because of the shadow.
      //     $('body').addClass('js-' + el + '-changed');
      //   }
      //   else {
      //     $('.' + el + ' a[data-actionevent]').addClass('button-disabled');
      //     $('body').removeClass('js-' + el + '-changed');
      //   }
      // });

      // observer.observe(layoutBuilder[0], {
      //   subtree: true,
      //   attributes: true
      // });
    }
  };
})(jQuery, Drupal, drupalSettings);
